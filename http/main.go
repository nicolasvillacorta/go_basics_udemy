package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

type logWriter struct{}

func main() {

	resp, err := http.Get("http://www.google.com")

	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1)
	}

	// La funcion read no puede resizear el slice, por eso  lo inicialize con lugares vacios.
	//		bs := make([]byte, 99999) // Crea un []byte con 99.999 espacios vacios.
	//		resp.Body.Read(bs)        // Lee el body dentro del []byte
	//		fmt.Println(string(bs))

	// Podemos con esta linea, ahorrarnos todos los pasos anteriores.
	//		io.Copy(os.Stdout, resp.Body) // La funcion recibe el writer (consola en este caso) y el reader de donde va a sacar la info.

	// Ejemplo con un custom writer.
	lw := logWriter{}
	io.Copy(lw, resp.Body)
}

func (logWriter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	fmt.Println("Just wrote this many bytes:", len(bs))
	return len(bs), nil
}
