package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

// Create a new custom type of 'deck'
// 	which is a slice of strings (will take all it's behaviour) deck === []string
type deck []string

func newDeck() deck {

	cards := deck{}

	cardSuits := []string{"Spades", "Diamonds", "Hearts", "Clubs"}
	cardValues := []string{"Ace", "Two", "Three", "Four"}

	for _, cardSuit := range cardSuits {
		for _, cardValue := range cardValues {
			cards = append(cards, cardValue+" of "+cardSuit)
		}
	}

	return cards
}

// Receiver, any variable of type "deck", now can access to this method
// 	* 'd' es una referencia a la copia actual de la variable de deck.
// 		por convencion usamos la primera o primeras 2/3 letras del tipo en el nombre
// 		de la variable, pero puede tener otro nombre. Podria llamarse this o self, pero por
// 		convencion no se hace.
// 	* 'deck' cualquier variable del tipo deck puede llamar esta funcion
func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

// En GO se pueden retornar mas de 1 value en una funcion
// d[:handSize] Devuelve todos los elementos hasta el indice handSize.
// d[handSize:] Devuelve todos los elementos a partir del handSize.
func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

// El permiso 0666 significa que cualquiera puede escribir o leer el archivo.
func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

// Si no hay error, la variable err va a recibir nil, es como un null.
func newDeckFromFile(filename string) deck {
	bs, err := ioutil.ReadFile(filename)

	if err != nil {
		// Option #1 - Log the error and return a call to newDeck().
		// Option #2 - Log the error and entirely quit the program.
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	s := strings.Split(string(bs), ",")
	return deck(s)
}

func (d deck) shuffle() {

	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {

		newPosition := r.Intn(len(d) - 1)

		d[i], d[newPosition] = d[newPosition], d[i]

	}
}
