package main

import "fmt"

func main() {

	// Hay 3 formas de crear mapas (siguientes 3 lineas, 2 vacios y uno con pairs)
	// 	var colors map[string]string
	// 	colors := make(map[int]string)
	colors := map[string]string{
		"red":   "#FF0000",
		"green": "#4BF745",
		"white": "#ffffff",
	}
	// Asi agrego values.
	// colors[10] = "my_value"
	// Asi borro values.
	// delete(colors, 10)

	printMap(colors)

}

func printMap(c map[string]string) {

	for color, hex := range c {
		fmt.Println("Hex code for", color, "is", hex)
	}

}
