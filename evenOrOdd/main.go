package main

import "fmt"

func main() {

	intSlice := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, integer := range intSlice {
		if integer%2 == 0 {
			fmt.Println(integer, " is even!!")
		} else {
			fmt.Println(integer, " is odd!")
		}
	}

}
