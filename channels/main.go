package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {

	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://stackoverflow.com",
		"http://golang.org",
		"http://amazon.com",
	}

	c := make(chan string) // Creo un channel de strings

	for _, link := range links {
		go checkLink(link, c)
	}

	// "range c" espera que el canal devuelva 1 valor y lo asigna a l, es infinito.

	for l := range c {

		go func(link string) {
			time.Sleep(5 * time.Second)
			checkLink(link, c)
		}(l)
		// Estos ultimos parentesis, son para, ademas de declarar la func, llamarla.
		// Si no le paso la L como argumento a la anonima, esta va a ejecutarse siempre
		// 		en el mismo link.
		// SIEMPRE PASEMOSLE LOS VALORES POR ARGUMENTO A LAS GO ROUTINES.
		// Sino va a apuntar a la direccion de memoria, que para cuando se ejecute
		//	probablemente va a ser el ultimo item del array.
	}

}

// Cuando llame a checkLink, espera 5 segundos y pingea denuevo.
// El channel se escribe en el argumento "'variable' chan 'type'"
func checkLink(link string, c chan string) {

	_, err := http.Get(link)

	if err != nil {
		fmt.Println(link, "might be down!")
		c <- link
		return
	}

	fmt.Println(link, "is up!")
	c <- link
}
